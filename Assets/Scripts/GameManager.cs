﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public GameObject objectToDeactivate;
    public SpriteMovement componentToDisable;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Keys to deactivate and reactivate the sprite, as well as closing the application
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }
        if (Input.GetKeyDown(KeyCode.P))
        {
            componentToDisable.enabled = !componentToDisable.enabled;
        }
        if (Input.GetKeyDown(KeyCode.Q))
        {
            objectToDeactivate.SetActive(!objectToDeactivate.activeInHierarchy);
        }
    }
}
